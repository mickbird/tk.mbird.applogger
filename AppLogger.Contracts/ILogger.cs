﻿using System;

namespace AppLogger
{
    public interface ILogger
    {
        void Log(string text);
        void LogLine(string text);

        void LogPrefix(string prefix, string text);
        void LogLinePrefix(string prefix, string text);
    }
}
