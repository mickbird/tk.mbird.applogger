﻿using System;

namespace AppLogger
{
    public class ConsoleLogger : ILogger
    {
        public void Log(string text)
        {
            Console.Write(text);
        }

        public void LogLine(string text)
        {
            Console.WriteLine(text);
        }

        public void LogLinePrefix(string prefix, string text)
            => this.LogLine($"{prefix} {text}");

        public void LogPrefix(string prefix, string text)
            => this.Log($"{prefix} {text}");
    }
}
